#include <stdio.h>
#include <string.h>
#include <sysexits.h>
#include "business.h"
#include "misc.h"
#include "config.h"

int main(int argc, char *argv[])
{
    if (argc > 1)
    {
        if (argc > 2)
        {
            fail();
            return EX_USAGE;
        }
        if (!strcmp(argv[1], "-h"))
        {
            help();
            return EX_OK;
        }
        if (!strcmp(argv[1], "-i"))
        {
            info();
            return EX_OK;
        }
        fail();
        return EX_USAGE;
    }
    puts("");
    if (SCHEME)
    {
        scheme(TEXT);
        puts("");
    }
    if (PALETTE)
    {
        palette(TEXT);
        puts("");
    }
    if (GREYSCALE)
    {
        greyscale(TEXT);
        puts("");
    }
    return EX_OK;
}
