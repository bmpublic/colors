#include <stdio.h>
#include "misc.h"

void fail(void)
{
    puts("wrong usage: Run colors -h for help.");
}

void help(void)
{
    puts("usage:");
    puts("colors       print color palette");
    puts("colors -h    print this help");
    puts("colors -i    print program info");
}

void info(void)
{
    puts("colors (Colors) 1.0");
    puts("Copyright (c) 2022 Benjamin Mross");
    puts("This program is distributed under the MIT license.");
}
