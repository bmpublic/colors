#include <stdio.h>
#include "business.h"

void scheme(const char * const text)
{
    short mode;
    short color;
    for (mode = 0; mode <= 2; mode++)
    {
        for (color = 0; color <= 7; color++)
        {
            printf("\033[%d;3%dm%s", mode, color, text);
        }
        puts("\033[0m");
    }
}

void palette(const char * const text)
{
    short offset;
    short color;
    offset = 16;
    for (color = 0; color <= 215; color++)
    {
        if (color != 0 && color % 6 == 0 && color % 18 != 0)
        {
            fputs("  ", stdout);
            offset += 30;
        }
        if (color != 0 && color % 18 == 0)
        {
            puts("");
            offset -= 72;
        }
        if (color != 0 && color % 108 == 0)
        {
            puts("");
            offset += 72;
        }
        printf("\033[0;38;5;%dm%s", color + offset, text);
    }
    puts("\033[0m");
}

void greyscale(const char * const text)
{
    short color;
    for (color = 232; color <= 255; color++)
    {
        printf("\033[0;38;5;%dm%s", color, text);
    }
    puts("\033[0m");
}
