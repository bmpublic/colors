# Colors
Prints the color palette of your terminal.

# Dependencies
**binutils** (to strip symbols from binary)

**gcc** (to compile the sources)

**make** (to use the makefile)

# Build
You can modify **src/h/config.h** or use the **CPPFLAGS** variable on Make.

To build and install the program you can use the makefile as follows:

    make
    make install

The makefile also includes some optional targets:

    make test
    make clean
    make uninstall

# Usage
Run the new command:

    colors
    colors -h
    colors -i
